

## Installation
### Requirements
- [Lando](https://docs.devwithlando.io)

### Recommended
- Composer 
    - This is installed inside of the container (and can be run as `lando composer`) but you should also install on your host machine
- NPM
    - This is installed inside of the container (and can be run as `lando npm`) but you should also install on your host machine
- wp-cli
    - This is installed inside of the container (and can be run as `lando wp`) but you should also install on your host machine
- [wp-cli Scaffold command](https://github.com/wp-cli/scaffold-command/)
    - This is installed inside of the container (and can be run as `lando wp scaffold`) but you should also install on your host machine

### Install
* `git clone https://gitlab.com/caldera-labs/gutenberg-examples/guten-dev`
* `cd guten-dev`
* `lando start`

#### What Does `lando start` Do?
##### First Start
* Creates local dev environment
* Installs WordPress, Gutenberg and example plugins
* Installs wp cli scaffold command
* Starts Server

##### First install
* Starts Server
* Updates Everything

## Usage

### Turn it on
 `lando start`

### Urls
* WordPress: https://guten-dev.lndo.site      
* Mailhog: https://mail.guten-dev.lndo.site 

### Add A New Plugin And Add A Block To It
* `lando wp scaffold plugin movies --path=wp`
* `lando wp scaffold plugin movies --path=wp`

Create a new plugin and add two blocks
* lando wp scaffold plugin books
* lando wp scaffold block book --title="Book" --plugin=books
* lando wp scaffold block books --title="Book List" --plugin=books
* lando wp plugin activate books/books.php

lando wp scaffold plugin books && lando wp scaffold block book --title="Book" && lando wp plugin activate books/books.php

