<?php

$rootPath = realpath( __DIR__  );


require_once( __DIR__ . '/vendor/autoload.php' );


$dotenv = new Dotenv\Dotenv( $rootPath );
$dotenv->load();



define( 'DB_NAME', 'wordpress' );
define( 'DB_USER', 'wordpress' );
define( 'DB_PASSWORD', 'wordpress' );
define( 'DB_HOST', 'database' );
define( 'DB_PORT', 3306 );
define( 'DB_CHARSET', 'utf8' );
define( 'DB_COLLATE', '' );

define( 'AUTH_KEY', '1' );
define( 'SECURE_AUTH_KEY', '1' );
define( 'LOGGED_IN_KEY', '1' );
define( 'NONCE_KEY', '1' );
define( 'AUTH_SALT', 1 );
define( 'SECURE_AUTH_SALT', 1 );
define( 'LOGGED_IN_SALT', 1 );
define( 'NONCE_SALT', 1);



define( 'WP_HOME', 'https://guten-dev.lndo.site'  );
define( 'WP_SITEURL', 'https://guten-dev.lndo.site/wp' );
define( 'WP_CONTENT_DIR', dirname( __FILE__ ) . '/wp-content' );
define( 'WP_CONTENT_URL', WP_HOME . '/wp-content' );

if ( ! defined( 'WP_DEBUG' )) {
    define('WP_DEBUG', true);
}
if ( ! defined( 'WP_DEBUG_LOG' )) {
    define('WP_DEBUG_LOG', true);
}



if( ! defined( 'WP_HOME' ) ){
    if( defined( 'WP_SITEURL' ) ){
        define( 'WP_HOME', WP_SITEURL );
    }
}

$table_prefix = getenv( 'DB_PREFIX' ) !== false ? getenv( 'DB_PREFIX' ) : 'wp_';


if ( ! defined( 'ABSPATH' ) ) {
    define( 'ABSPATH', dirname( __FILE__ ) . '/' );
}
/** Sets up WordPress vars and included files. */
require_once( ABSPATH . 'wp-settings.php' );