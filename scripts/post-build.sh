#!/usr/bin/env bash
rm ../wp/wp-config.php
lando wp package install git@github.com:wp-cli/scaffold-command.git --path=wp
lando wp package install git@github.com:wp-cli/scaffold-package-command.git --path=wp
lando wp option update permalink_structure '/%postname%/' --path=wp
lando wp package install git@github.com:wp-cli/scaffold-package-command.git

bash ./post-start.sh


