#!/usr/bin/env bash
lando wp plugin activate gutenberg/gutenberg.php --path=wp
lando wp plugin activate ex2-vue/ex2-vue.php --path=wp
lando wp plugin activate ex2-plainjs/ex2-plainjs.php --path=wp
lando wp plugin activate ex2-react/ex2-react.php --path=wp
lando wp plugin activate ex2-event-block/ex2-event-block.php --path=wp
git clone https://github.com/youknowriad/gcf && npm install && npm update