//Save callback for block
save: function ({attributes, className}) {
    return el(
        //Output everything wrapped in one "div"
        'div',
        {
            //All elements must be decedents of this element
            className: className,
        },
        [
            //Create element to save attribute in
            el(
                'p',
                {},
                //Set innerHTML -- IE content of paragraph to be the block attribute "name"
                attributes.name
            ),
        ]
    );
}


